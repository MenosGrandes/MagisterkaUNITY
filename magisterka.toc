\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Cel, za\IeC {\l }o\IeC {\.z}enia i zakres pracy}{3}{section.1.1}
\contentsline {chapter}{\numberline {2}System Lindermayera}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Fraktal}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}System Lindermayera}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}DOL-system}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Parametryczny L-system}{9}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Nawiasowy L-system}{11}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Geometryczna interpretacja s\IeC {\l }\IeC {\'o}w przy pomocy grafiki \IeC {\.z}\IeC {\'o}\IeC {\l }wiowej}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Przestrze\IeC {\'n} 2D}{12}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Przestrze\IeC {\'n} 3D}{13}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Krzywe B-sklejane}{14}{section.2.4}
\contentsline {chapter}{\numberline {3}Przegl\IeC {\k a}d istniej\IeC {\k a}cej literatury i rozwi\IeC {\k a}za\IeC {\'n}}{16}{chapter.3}
\contentsline {section}{\numberline {3.1}Wst\IeC {\k e}p}{16}{section.3.1}
\contentsline {section}{\numberline {3.2}Programy generuj\IeC {\k a}ce ro\IeC {\'s}linno\IeC {\'s}\IeC {\'c}}{17}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}SpeedTree}{17}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Houdini}{18}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Algorytm SpaceColonization}{18}{section.3.3}
\contentsline {section}{\numberline {3.4}Prace wykorzystuj\IeC {\k a}ce krzywe Beziera}{20}{section.3.4}
\contentsline {chapter}{\numberline {4}Praca praktyczna}{21}{chapter.4}
\contentsline {section}{\numberline {4.1}Interfejs u\IeC {\.z}ytkownika}{21}{section.4.1}
\contentsline {section}{\numberline {4.2}Implementacja L-systemu}{24}{section.4.2}
\contentsline {section}{\numberline {4.3}Przyk\IeC {\l }ady}{32}{section.4.3}
\contentsline {section}{\numberline {4.4}Podsumowanie oraz wnioski}{37}{section.4.4}
\contentsline {section}{\numberline {4.5}Dalszy rozw\IeC {\'o}j pracy}{37}{section.4.5}
\contentsline {chapter}{Bibliografia}{38}{section.4.5}
\contentsline {chapter}{Spis rysunk\IeC {\'o}w}{40}{chapter*.25}
\contentsline {chapter}{Spis kod\IeC {\'o}w programu}{41}{chapter*.25}
\contentsline {chapter}{Lista r\IeC {\'o}wna\IeC {\'n}}{42}{chapter*.26}
